import {Container, Row, Col, Card, Button} from 'react-bootstrap';

// import use state hook from react
import {useState, useEffect} from 'react';

import {useContext} from 'react';

import UserContext from '../UserContext.js';

import {Link} from 'react-router-dom';


export default function CourseCard(props) {

	// consume the content of the UserContext.
	const {user} = useContext(UserContext);

	console.log(props.courseProp)

	// object desctructuring

	const{_id, name, description, price} = props.courseProp;

	// Use the state hook for this component to be able to store its state.
	// States are use to keep track of information related to individual components

		// Syntax: const [getter, setter] = useState(initialGetterValue);

	const [count, setCount] = useState(0);

	const [seat, setSeat] = useState(30)

	const [isDisabled, setIsDisabled] = useState(false);

	// console.log(count);
	// console.log(seat);

	// setCount(2);
	// console.log(count);

	// this function will be invoke when the button enroll is clicked
	function enroll(){


		if (seat !== 0){

			setCount(count + 1);

			setSeat(seat - 1);

			
		}else{

			alert("No more available slots!")

		}

	}

	// the function or the side-effect in our useEffect will invoke or run on the initial loading of our application and when there is/are change/changes on our dependencies.

	// Syntax:
		// useEffect(sideEffect, [dependencies]);
	useEffect(() => {

		if(seat === 0){
			setIsDisabled(true);
		}

	}, [seat]);


	return(
		<Container>
			<Row>
				<Col className="col-12 mt-3">
					<Card>
					    <Card.Body>
					    	<Card.Title className="mb-3">{props.courseProp.name}</Card.Title>
					    	<Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>

					        <Card.Subtitle>Enrollees:</Card.Subtitle>
					        <Card.Text>{count}</Card.Text>

					        <Card.Subtitle>Seats:</Card.Subtitle>
					        <Card.Text>{seat}</Card.Text>

					        {
					        	user !== null
					        	?
					        	<Button as = {Link} to = {`/courses/${_id}`}>Details</Button>
					        	:
					        	<Button as = {Link} to = '/login'>Login to enroll!</Button>
					        }
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	);
}
