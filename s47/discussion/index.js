// console.log("hello");

// Section: Document Object Model (DOM)
	// allows us to access or modify the properties of an html element in a web page.
	// it is standard on how to get, change, add or delete HTML elements.
	// we will be focusing only with DOM in terms of managing forms.

	// using the querySelector it can access the HTML element/s 
		// CSS selectors to target specific element:
			// id selector(#);
			// class selector(.);
			// tag/type selector(html tags);
			// universal selector(*);
			// attribute selector([attribute]);

	// Query selectors has two types: querySelector and querySelectorAll.

let firstElement = document.querySelector('#txt-first-name');
// console.log(firstElement);

	// querySelector
	let secondElement = document.querySelector('.full-name');
	console.log(secondElement);

	// querySelectorAll
	let thirdElement = document.querySelectorAll('.full-name');
	console.log(thirdElement);

	// getElements
	// using getElementById
	let element = document.getElementById('fullName');
	console.log(element);

	// using getElementsByClassName
	element = document.getElementsByClassName('fullName');
	console.log(element);


// Section: Event Listeners
	// whenever a user interacts with a webpage, this action is consider as event.
	// working with events is large part of creating interactivity in a webpage.
	// specific function will be invoked if the even happen.

	// function addEventListener, it takes two arguments.
		// first argument is a string identifying the even.
		// second argument function that the listener invoke once the specified event occur.

	let fullName = document.querySelector('#fullName');

	let txtLastName = document.querySelector('#txt-last-name');

	let fullNameColor = document.querySelector('#text-color');
	

	firstElement.addEventListener('keyup', () => {
		console.log(firstElement.value);

		fullName.innerHTML = `${firstElement.value} ${txtLastName.value}`
	})

	txtLastName.addEventListener('keyup', () => {
		console.log(txtLastName.value);

		fullName.innerHTML = `${firstElement.value} ${txtLastName.value}`
	})

	fullNameColor.addEventListener('change', () => {


		fullName.style.color = fullNameColor.value;
	})


	/*element.forEach(elmnt => {
		elmnt.addEventListener()
	})*/
